import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { ellipse, square, triangle, homeOutline, listOutline, fileTrayFullOutline } from 'ionicons/icons';
import Welcome from './pages/Welcome';
import PromotionPage from './pages/PromotionsPage';
import YourBooking from './pages/YourBooking';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import PromotionDetailPage from './pages/PromotionDetailPage';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path='/welcome'>
            <Welcome />
          </Route>
          <Route exact path='/promotions' component={PromotionPage} />
          <Route path='/promotion/:id' component={PromotionDetailPage} />

          <Route exact path='/your-booking' component={YourBooking} />
          {/* <Route path='/your-booking/:id' component={PromotionDetail} /> */}
          <Route exact path='/'>
            <Redirect to='/welcome' />
          </Route>
        </IonRouterOutlet>
        <IonTabBar slot='bottom'>
          <IonTabButton tab='welcome' href='/welcome'>
            <IonIcon icon={homeOutline} />
            <IonLabel>Welcome</IonLabel>
          </IonTabButton>
          <IonTabButton tab='promotions' href='/promotions'>
            <IonIcon icon={listOutline} />
            <IonLabel>Promotions</IonLabel>
          </IonTabButton>
          <IonTabButton tab='your-booking' href='/your-booking'>
            <IonIcon icon={fileTrayFullOutline} />
            <IonLabel>You booking</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
