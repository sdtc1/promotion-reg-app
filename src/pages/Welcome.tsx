import {
  IonContent,
  IonHeader,
  IonItemDivider,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import './Welcome.css';

const Welcome: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          {/* <IonTitle>Welcome</IonTitle> */}
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div
          style={{
            width: '100%',
            height: '200px',
            position: 'absolute',
            top: '40%',
            marginTop: '-100px',
            textAlign: 'center',
          }}
        >
          <h1>Welcome</h1>
          <h1>Test user</h1>
          <span>(No Authentication system)</span>
          <h3>(Beta version for SDTC research)</h3>
          <IonItemDivider/>
          <h4>PATOMPONG SULSAKSAKUL - 30668</h4>
          <h4>OAMMARIN REPHON - 30062</h4>
          <h5>Major: Computer Programmer</h5>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Welcome;
