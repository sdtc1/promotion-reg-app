import {
  IonCard,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonThumbnail,
  IonImg,
  useIonViewWillEnter
} from "@ionic/react";
import { Redirect } from "react-router";
import { Link } from "react-router-dom";
import "./YourBooking.css";
import { gellAllBookedList } from "../services/promotionService";
import { useEffect, useState } from "react";
import moment from "moment";

const YourBooking: React.FC = () => {
  useIonViewWillEnter(() => {
    loadBookedList();
  }, []);

  const [bookedList, setBookedList] = useState<any[]>([]);

  async function loadBookedList() {
    setBookedList(await gellAllBookedList());
  }

  const handleCardClick = () => {
    console.log("Clicked");
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Your Booking</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        {bookedList &&
          bookedList.map((booked) => (
            <IonCard onClick={handleCardClick}>
              <IonCardHeader>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div>
                    <IonCardSubtitle>
                      Booked date:{" "}
                      {moment(booked.date.toDate()).format("DD-MMM-YYYY")}
                    </IonCardSubtitle>
                    <IonCardTitle>Hotel</IonCardTitle>
                  </div>
                </div>
              </IonCardHeader>
              <IonCardContent>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div>
                    <p>{booked.title}</p>
                  </div>
                  <div>
                    <p>By {booked.provider}</p>
                  </div>
                </div>
                <div style={{marginBottom: 0}}>
                  <p>Booked by: {booked.name}</p>
                </div>
              </IonCardContent>
            </IonCard>
          ))}
      </IonContent>
    </IonPage>
  );
};

export default YourBooking;
