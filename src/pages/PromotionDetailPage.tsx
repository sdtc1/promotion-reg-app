import {
  IonCard,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonItem,
  IonLabel,
  IonText,
  IonItemGroup,
  IonInput,
  IonButton,
  NavContext,
  IonIcon,
  IonImg,
  useIonViewWillEnter
} from "@ionic/react";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { RouteComponentProps } from "react-router";
import "./PromotionDetailPage.css";
import { add, caretBackOutline } from "ionicons/icons";
import {
  getPromotionById,
  IPromotion,
  addBookedList,
} from "../services/promotionService";
import moment from "moment";
import firebase from "firebase";

interface PromotionDetailPageProps
  extends RouteComponentProps<{
    id: string;
  }> {}

interface PromotionForm {
  name: string;
  phone: string;
  bookedDate: string;
}

const PromotionDetailPage: React.FC<PromotionDetailPageProps> = ({
  match,
  history,
}) => {
  const [promotion, setPromotion] = useState<IPromotion>();
  const [form, setForm] = useState<PromotionForm>({
    bookedDate: moment().format("YYYY-MM-DD"),
    name: "",
    phone: "",
  });

  useIonViewWillEnter(() => {
    loadCurrentPromotion();
  }, []);

  async function loadCurrentPromotion() {
    const data = await getPromotionById(match.params.id);
    setPromotion(data);
  }

  const handleBooking = async () => {
    await addBookedList(match.params.id, {
      bookDate: firebase.firestore.Timestamp.fromDate(
        moment(form.bookedDate).toDate()
      ),
      name: form.name,
      phone: form.phone,
    });

    history.push("/your-booking");
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>
            <IonIcon icon={caretBackOutline} onClick={history.goBack}></IonIcon>{" "}
            Promotion info..
          </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div>
          <h1 style={{ marginTop: "32px" }}>Information</h1>
          <IonCard>
            <IonCardHeader>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <IonCardSubtitle>{promotion?.category}</IonCardSubtitle>
                  <IonCardTitle>{promotion?.title}</IonCardTitle>
                </div>
                <div style={{ textAlign: "right" }}>
                  <span>
                    {moment(promotion?.expiredAt.toDate()).format(
                      "DD-MMM-YYYY"
                    )}
                  </span>
                  <p>
                    {moment(moment(promotion?.expiredAt.toDate())).diff(
                      new Date(),
                      "days"
                    )}{" "}
                    days left
                  </p>
                </div>
              </div>
            </IonCardHeader>
            <IonImg src={promotion?.coverImageUrl} />
            <IonCardContent>{promotion?.content}</IonCardContent>
            <IonCardContent>
              <p>Price: {promotion?.price}</p>
            </IonCardContent>
            <IonCardContent style={{ textAlign: "right", fontWeight: "bold" }}>
              By {promotion?.provider}
            </IonCardContent>
          </IonCard>
        </div>
        <div style={{ margin: "16px" }}>
          <IonItem>
            <IonLabel position="stacked">Name:</IonLabel>
            <IonInput
              value={form.name}
              placeholder={"Your full name"}
              onIonChange={(e) =>
                setForm({ ...form, name: e.detail.value as string })
              }
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel position="stacked">Phone no:</IonLabel>
            <IonInput
              value={form.phone}
              placeholder={"08X-XXX-XXXX"}
              onIonChange={(e) =>
                setForm({ ...form, phone: e.detail.value as string })
              }
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel position="stacked">Book date:</IonLabel>
            <IonInput
              type="date"
              value={form.bookedDate}
              onIonChange={(e) =>
                setForm({ ...form, bookedDate: e.detail.value as string })
              }
            ></IonInput>
          </IonItem>
        </div>
        <div style={{ margin: "32px 16px 32px 16px" }}>
          <IonButton expand="block" fill="outline" onClick={handleBooking}>
            Confirm
          </IonButton>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default PromotionDetailPage;
