import {
  IonCard,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonThumbnail,
  IonImg,
  useIonViewWillEnter
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router";
import "./Promotions.css";
import { getAllPromotions, IPromotion } from "../services/promotionService";
import moment from "moment";

const PromotionPage: React.FC<RouteComponentProps> = ({ history }) => {
  const handleCardClick = (id: string) => {
    history.push("/promotion/" + id);
  };

  const [promotions, setPromotions] = useState<IPromotion[]>([]);

  useIonViewWillEnter(() => {
    loadAllPromotions();
  }, []);

  async function loadAllPromotions() {
    setPromotions(await getAllPromotions());
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>All Promotions</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        {promotions.map((promotion) => (
          <IonCard
            key={promotion.id}
            onClick={() => handleCardClick(promotion.id)}
          >
            <IonCardHeader>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <IonCardSubtitle>{promotion.category}</IonCardSubtitle>
                  <IonCardTitle>{promotion.title}</IonCardTitle>
                </div>
                <div>
                  <span>
                    {moment(promotion.expiredAt.toDate()).format("DD-MMM")}
                  </span>
                  <p>
                    {moment(moment(promotion.expiredAt.toDate())).diff(
                      new Date(),
                      "days"
                    )}{" "}
                    days
                  </p>
                </div>
              </div>
            </IonCardHeader>
            <IonImg src={promotion.coverImageUrl} />

            <IonCardContent>
              {promotion.content.substring(0, 200)}...
            </IonCardContent>
            <IonCardContent>
              <p>Price: {promotion.price}</p>
            </IonCardContent>
            <IonCardContent style={{ textAlign: "center", fontWeight: "bold" }}>
              {promotion.provider}
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
    </IonPage>
  );
};

export default PromotionPage;
