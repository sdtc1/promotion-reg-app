import {
  IonCard,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonItem,
  IonLabel,
  IonText,
  IonItemGroup,
  IonInput,
  IonButton,
  NavContext,
} from '@ionic/react';
import React, { useCallback, useContext } from 'react';
import { RouteComponentProps } from 'react-router';
import './PromotionDetailPage.css';

interface PromotionDetailPageProps
  extends RouteComponentProps<{
    id: string;
  }> {}

const PromotionDetailPage: React.FC<PromotionDetailPageProps> = ({
  match,
  history,
}) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>
            <span onClick={history.goBack}>{'<'} </span>
            Promotion info..
          </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div>
          <h1 style={{ marginLeft: '16px', marginTop: '32px' }}>Information</h1>
          <IonCard>
            <IonCardHeader>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div>
                  <IonCardSubtitle>HOTEL</IonCardSubtitle>
                  <IonCardTitle>Centara – The Place to Be</IonCardTitle>
                </div>
                <div style={{ textAlign: 'right' }}>
                  <span>24 Feb 2020</span>
                  <p>61 days</p>
                </div>
              </div>
            </IonCardHeader>
            <IonCardContent>
              Whether you’re planning a sun-drenched holiday by the beach or an
              urban staycation in a city that never sleeps, Centara is The Place
              to Be. Start your days with daily breakfast, and experience more
              and relax deeper with free hotel credits redeemable for dining and
              spa indulgences during your stay. Book by 31 March 2021 and stay
              by 30 June 2021 to enjoy: Special room ratesfrom THB 765/night
              Hotel credit up to THB 1,400/night Daily breakfastfor 2 persons*
              Free stay for up to 2 children** Free Wi-Fi
            </IonCardContent>
            <IonCardContent style={{ textAlign: 'right', fontWeight: 'bold' }}>
              By Centara Grand
            </IonCardContent>
          </IonCard>
        </div>
        <div style={{ margin: '16px' }}>
          <IonItemGroup>
            <IonText>
              <h3>Name:</h3>
            </IonText>
            <IonItem>
              <IonLabel>Wipaporn Tangsaksomwang</IonLabel>
            </IonItem>
          </IonItemGroup>
          <IonItemGroup>
            <IonText>
              <h3>Phone no:</h3>
            </IonText>
            <IonItem>
              <IonLabel>0812345678</IonLabel>
            </IonItem>
          </IonItemGroup>
          <IonItemGroup>
            <IonText>
              <h3>Book date:</h3>
            </IonText>
            <IonItem>
              <IonLabel>24-02-2020</IonLabel>
            </IonItem>
          </IonItemGroup>
        </div>
        <div style={{ margin: '32px 16px 32px 16px' }}>
          <IonButton expand='block' fill='outline'>
            Confirm
          </IonButton>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default PromotionDetailPage;
