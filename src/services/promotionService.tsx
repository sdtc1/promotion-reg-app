import db from './firebaseInitialize';
import moment from 'moment';
import firebase from 'firebase';

export interface IPromotion {
  id: string;
  category: string;
  title: string;
  expiredAt: firebase.firestore.Timestamp;
  content: string;
  provider: string;
  coverImageUrl: string;
  price: string;
  booked: [IUser];
}

export interface IUser {
  name: string;
  phone: string;
  bookDate: firebase.firestore.Timestamp;
}

export const addNewPromotion = (promotion: IPromotion) => {
  console.log('AddNewPromotion Called');
  db.collection('promotion').add({
    category: 'Hotel',
    title: 'Centara – The Place to Be',
    expiredAt: firebase.firestore.Timestamp.fromDate(
      moment(Date.now()).add(45, 'days').toDate()
    ),
    content:
      'Whether you’re planning a sun-drenched holiday by the beach or an urban staycation in a city that never sleeps, Centara is The Place to Be. Start your days with daily breakfast, and experience more and relax deeper with free hotel credits redeemable for dining and spa indulgences during your stay. Book by 31 March 2021 and stay by 30 June 2021 to enjoy: Special room ratesfrom THB 765/night Hotel credit up to THB 1,400/night Daily breakfastfor 2 persons* Free stay for up to 2 children** Free Wi-Fi',
    provider: 'Centara Grand',
    booked: [
      {
        name: 'Patompong Sulsaksakul',
        phone: '999-9999',
        bookDate: firebase.firestore.Timestamp.now(),
      },
    ],
  });
};

export const getAllPromotions = async () => {
  const query = await db.collection('promotion').get();
  const promotions = query.docs.map((x) => {
    return {
      ...x.data(),
      id: x.id,
    } as IPromotion;
  });

  return promotions;
};

export const getPromotionById = async (id: string) => {
  const query = await db.collection('promotion').doc(id).get();

  return query.data() as IPromotion;
};

export const addBookedList = async (id: string, user: IUser) => {
  const query = await db.collection('promotion').doc(id).get();
  const currentData = query.data() as IPromotion;

  var bookedList = currentData.booked;
  bookedList.push(user);

  await query.ref.update({ booked: bookedList });
};

export const gellAllBookedList = async () => {
  const query = await db.collection('promotion').get();

  const bookedList: any[] = [];
  query.docs.map((p) => {
    p.data().booked.map((b: any) => {
      bookedList.push({
        name: b.name,
        phone: b.phone,
        date: b.bookDate,
        title: p.data().title,
        provider: p.data().provider,
      });
    });
  });

  return bookedList;
};
