import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyDVXmQ6VjtssQNq6vxKF_8Ri12Tc19VWhw',
  authDomain: 'sdtc-reg-app.firebaseapp.com',
  projectId: 'sdtc-reg-app',
  storageBucket: 'sdtc-reg-app.appspot.com',
  messagingSenderId: '911196985958',
  appId: '1:911196985958:web:4f2ad2a69c5d5022fa2ba7',
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export default db;